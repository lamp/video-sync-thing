var express = require("express");
var socketio = require("socket.io");
var MediaPlayer = require("./mediaplayer");

var app = express();
var server = app.listen(1234);
var io = socketio(server);

app.use(express.static("."));


var player = new MediaPlayer();
player.setMedia("media/nyan_cat.webm").then(() => {
	player.play();
});

player._emit = player.emit;
player.emit = function() {
	console.debug("player emit", arguments);
	player._emit.apply(player, arguments);
}

player.on("end", () => setTimeout(() => player.play(), 5000));


io.on("connection", socket => {
	socket.on("syncme", () => {
		socket.emit("media", player.file);
		socket.emit("pos", player.position);
		if (player.playing) socket.emit("play");
		else socket.emit("pause");
	});
	socket.on("reqpos", () => socket.emit("pos", player.position));
	socket.on("c_pos", pos => player.position = pos);
	socket.on("c_pause", () => player.pause());
	socket.on("c_play", () => player.play());
});

player.on("media", file => io.emit("media", file));
player.on("play", () => io.emit("play"));
player.on("pause", () => io.emit("pause"));
player.on("pos", pos => io.emit("pos", pos));
player.on("end", () => io.emit("end"));



global.player = player;