var MediaPlayer = require("./mediaplayer");

global.mp = new MediaPlayer();
mp.setMedia(undefined, 10000);
mp.on("play", () => console.log("play"));
mp.on("pause", () => console.log("pause"));
mp.on("media", url => console.log("media", url));
mp.on("end", () => console.log("end"));